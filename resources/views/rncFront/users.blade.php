@extends('rncFront.master')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm" id="dataTable" style="text-align:center">
                        <thead>
                            <tr style="font-size: 11px; text-align:center">
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Empresa</th>
                                <th>Perfil</th>
                                <th>Alterou Senha</th>
                                <th>Created_at</th>
                                <th>Updated_at</th>
                            </tr>

                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr style="font-size: 11px; text-align:center">
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->empresa }}</td>
                                    <td>{{ $user->perfil }}</td>
                                    @if (!empty($user->remember_token))
                                        <td>Sim</td>
                                    @else
                                        <td>Não</td>
                                    @endif

                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->updated_at }}</td>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>

    <!-- /.container-fluid -->
@endsection
