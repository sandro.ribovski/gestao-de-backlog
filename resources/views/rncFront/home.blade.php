@extends('rncFront.master')

@section('content')
    <div id="content">
        {!! Toastr::message() !!}
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                        class="fas fa-download fa-sm text-white-50"></i> Exportar</a>
            </div>
                       
          
            <!-- Content Row -->
            @if(Auth::user()->empresa == "Ligga Telecom")       
                @include('rncFront.card')
            @endif
            <!-- Content Row -->

            <div class="row">

                <!-- Area Chart -->
                <div class="col-xl-8 col-lg-7">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Aberturas Diária</h6>

                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="chart-area">
                                <canvas id="myAreaChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Pie Chart -->
                <div class="col-xl-4 col-lg-5">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Tipo de Aplicação</h6>
                            <div class="dropdown no-arrow">

                            </div>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="chart-pie pt-4 pb-2">
                                <!-- <canvas id="myPieChart"></canvas> -->
                                <canvas id="aa"></canvas>
                            </div>
                            <div class="mt-4 text-center small">
                                <span class="mr-2">
                                    <i class="fas fa-circle text-primary"></i> Emergencial
                                </span>
                                <span class="mr-2">
                                    <i class="fas fa-circle text-success"></i> Não Emergencial
                                </span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Content Row -->
            <div class="row">
                <!-- Content Column -->
                <div class="col-lg-12 mb-4">
                    <!-- Project Card Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Ofensor por Atividades</h6>
                        </div>
                        <div class="card-body">
                            @foreach (qtdOfensores() as $atividade)
                                <h4 class="small font-weight-bold">{{ $atividade->atividade . ': ' }}<span
                                        class="float-right">{{ $atividade->perct . '%' }}</span>
                                </h4>
                                <div class="progress mb-4">
                                    <div class="progress-bar {{ progressClasse($atividade->perct) }}" role="progressbar"
                                        style="width: {{ $atividade->perct . '%' }}" aria-valuenow="20" aria-valuemin="0"
                                        aria-valuemax="100"><span
                                            class="float-right font-weight-bold">{{ $atividade->qtd }}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->
@endsection
