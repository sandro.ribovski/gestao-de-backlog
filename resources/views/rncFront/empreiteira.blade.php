@extends('rncFront.master')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        @include('rncFront.card')
        <!-- DataTales Example -->
        <div class="card shadow mb-4">

            @include('rncFront.formFiltro')

            <div class="card-body">
                <div class="table-responsive">
                    <table data-order='[[ 1, "desc" ]]' class="table table-bordered table-sm" id="dataTable" style="text-align:center">
                        <thead>
                            <tr style="font-size: 11px; text-align:center">
                                <th>Contrato</th>
                                <th>Data Venda</th>
                                <th>Data Promessa</th>
                                <th>Data Agendada</th>
                                <th>Periodo Agendado</th>
                                <th>Dias Backlog</th>
                                <th>Dias Atraso</th>
                                <th>Cidade</th>
                                <th>Polo</th>
                                <th>Regional</th>
                                <th>Id Atividade</th>
                                <th>Tipo Atividade</th>
                                <th>Status Atividade</th>
                                <th>Tecnico</th>
                                <th>Tipo Imovel</th>
                                <th>Cliente</th>
                                <th>Fone</th>
                                <th>Celular</th>
                            </tr>

                        </thead>
                        <tbody>
                            @foreach ($backlog as $ativ)
                                <tr style="font-size: 11px; text-align:center">
                                    <td>{{ $ativ->contrato }}</td>
                                    <td>{{ formatarData($ativ->data_da_venda) }}</td>
                                    <td>{{ formatarData($ativ->data_promessa) }}</td>
                                    <td>{{ formatarData($ativ->data_agendada) }}</td>
                                    <td>{{ $ativ->periodo_agendado }}</td>
                                    <td>{{ $ativ->dias_backlog }}</td>
                                    <td>{{ $ativ->dias_atraso }}</td>
                                    <td>{{ $ativ->cidade }}</td>
                                    <td>{{ $ativ->polo }}</td>
                                    <td>{{ $ativ->regional }}</td>
                                    <td>{{ $ativ->id_atividade }}</td>
                                    <td>{{ $ativ->tipo_atividade }}</td>
                                    <td>{{ $ativ->status_atividade }}</td>
                                    <td>{{ $ativ->tecnico }}</td>
                                    <td>{{ $ativ->tipo_imovel }}</td>
                                    <td>{{ $ativ->cliente }}</td>
                                    <td>{{ $ativ->fone }}</td>
                                    <td>{{ $ativ->celular }}</td>



                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>

    <!-- /.container-fluid -->
@endsection
