<div class="collapse" id="collapseExample">
    <div class="card-body">
        
        <form action="{{ route('filtro') }}" method="POST">
            @csrf
            <div class="row container-fluid">
                <fieldset class="well border container-fluid">
                    <legend class="well-legend font-weight-bold">Dados do Filtro:</legend>
                    <!--
                    <div class="row row-cols-2">
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Venda Inicio:</span>
                                </div>
                                <input type="date" class="form-control" name="data_da_venda">
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Venda Fim:</span>
                                </div>
                                <input type="date" class="form-control" name="data_da_venda_fim">
                            </div>
                        </div>
                    </div>
                    <div class="row row-cols-2">
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Promessa Inicio:</span>
                                </div>
                                <input type="date" class="form-control" name="data_promessa">
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Promessa Fim:</span>
                                </div>
                                <input type="date" class="form-control" name="data_promessa_fim">
                            </div>
                        </div>
                    </div>
                    <div class="row row-cols-2">
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Agendada Inicio:</span>
                                </div>
                                <input type="date" class="form-control" name="data_agendada">
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Data da Agendada Fim:</span>
                                </div>
                                <input type="date" class="form-control" name="data_agendada_fim">
                            </div>
                        </div>
                    </div>
                    <div class="row row-cols-2">
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Dias Backlog Inicio:</span>
                                </div>
                                <input type="number" class="form-control" name="dias_backlog">
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Dias Backlog Fim:</span>
                                </div>
                                <input type="number" class="form-control" name="dias_backlog_fim">
                            </div>
                        </div>
                    </div>
                    <div class="row row-cols-2">
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Dias Atraso Inicio:</span>
                                </div>
                                <input type="number" class="form-control" name="dias_atraso">
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Dias Atraso Fim:</span>
                                </div>
                                <input type="number" class="form-control" name="dias_atraso_fim">
                            </div>
                        </div>
                    </div>
                -->
                    <div class="row row-cols-3">
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Cidade:</span>
                                </div>
                                <select type="text" class="form-control" name="cidade">
                                    <option value="" selected>Sel</option>
                                    @foreach (listCidades() as $key)
                                        <option value="{{ $key->cidade }}">
                                            {{ mb_convert_case($key->cidade, MB_CASE_TITLE, 'UTF-8') }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Polo:</span>
                                </div>
                                <select type="text" class="form-control" name="polo">
                                    <option value="" selected>Sel</option>
                                    @foreach (listPolos() as $key)
                                        <option value="{{ $key->polo }}">
                                            {{ mb_convert_case($key->polo, MB_CASE_TITLE, 'UTF-8') }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Regional:</span>
                                </div>
                                <select type="text" class="form-control" name="regional">
                                    <option value="" selected>Sel</option>
                                    <option value="CTA">CTA</option>
                                    <option value="CEL">CEL</option>
                                    <option value="LNA">LNA</option>
                                    <option value="MGA">MGA</option>
                                    <option value="PGO">PGO</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row row-cols-4">

                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Recurso na Venda:</span>
                                </div>
                                <select type="text" class="form-control" name="recurso_disp_na_venda">
                                    <option value="" selected>Sel</option>
                                    <option value="Sim">Sim</option>
                                    <option value="Não">Não</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Fluxo Atual:</span>
                                </div>
                                <select type="text" class="form-control" name="fluxo_atual">
                                    <option value="" selected>Sel</option>
                                    <option value="Lisa">Lisa</option>
                                    <option value="Ampliacao">Ampliacao</option>
                                    <option value="Criar Lote">Criar Lote</option>
                                    <option value="Prumada">Prumada</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Tipo Atividade:</span>
                                </div>
                                <select type="text" class="form-control" name="tipo_atividade">
                                    <option value="" selected>Sel</option>
                                    <option value="Ativação">Ativação</option>
                                    <option value="Mud End">Mudança de Endereço</option>                                  
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Status Atividade:</span>
                                </div>
                                <select type="text" class="form-control" name="status_atividade">
                                    <option value="" selected>Sel</option>
                                    <option value="Não Agendado">Não Agendado</option>
                                    <option value="Não iniciada">Não iniciada</option>
                                    <option value="Cancelada">Cancelada</option>
                                    <option value="Iniciada">Iniciada</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row row-cols-4">
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Backlog Empreiteira:</span>
                                </div>
                                <select type="text" class="form-control" name="backlog_empreiteira">
                                    <option value="" selected>Sel</option>
                                    <option value="sim">Sim</option>
                                    <option value="nao">Não</option>
                                </select>
                            </div>
                        </div>

                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Agendado:</span>
                                </div>
                                <select type="text" class="form-control" name="agendada">
                                    <option value="" selected>Sel</option>
                                    <option value="Sim">Sim</option>
                                    <option value="Não">Não</option>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Despachado:</span>
                                </div>
                                <select type="text" class="form-control" name="despachada">
                                    <option value="" selected>Sel</option>
                                    <option value="Sim">Sim</option>
                                    <option value="Não">Não</option>
                                    
                                </select>
                            </div>
                        </div>

                        <div class="col">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Empreiteira:</span>
                                </div>
                                <select type="text" class="form-control" name="empreiteira">
                                    <option value="" selected>Sel</option>
                                    @foreach (listEmpreiteira() as $key)
                                        <option value="{{ $key->empreiteira }}">
                                            {{ mb_convert_case($key->empreiteira, MB_CASE_TITLE, 'UTF-8') }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-icon-split"> <span class="icon text-white-50">
                            <i class="fas fa-arrow-right"></i>
                        </span>
                        <span class="text">Filtrar</span>
                    </button>
                </fieldset>
            </div>
        </form>
    </div>
</div>
