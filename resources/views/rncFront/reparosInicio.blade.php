@extends('rncFront.master')

@section('content')
    <div style="zoom: 77%">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="container-fluid">
                <!-- Content Row -->
                <div class="row">
                    <!-- First Column -->
                    <div class="col-xl-3 col-md-6">
                        <div class="card border-left-warning shadow h-10 py-2 ">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="mb-0 font-weight-bold text-warning text-uppercase mb-1">CRM
                                        </div>
                                        <div class="mb-0 font-weight-bold font-weight-bold text-warning">2544
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="mb-0 mr-3 font-weight-bold text-warning">50%</div>
                                            </div>
                                            <div class="col">
                                                <div class="progress progress-sm mr-2">
                                                    <div class="progress-bar bg-warning" role="progressbar"
                                                        style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                        aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">

                                        <i class="fa-solid fa-headset fa-2x text-gray-500"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Second Column -->
                    <div class="col-xl-3 col-md-6">
                        <div class="card border-left-primary shadow h-10 py-2 ">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="mb-0 font-weight-bold text-primary text-uppercase mb-1">CTC
                                        </div>
                                        <div class="mb-0 font-weight-bold font-weight-bold text-primary">2544
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="mb-0 mr-3 font-weight-bold text-primary">26%</div>
                                            </div>
                                            <div class="col">
                                                <div class="progress progress-sm mr-2">
                                                    <div class="progress-bar bg-primary" role="progressbar"
                                                        style="width: 86%" aria-valuenow="50" aria-valuemin="0"
                                                        aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">

                                        <i class="fa-solid fa-desktop fa-2x text-gray-500"></i>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-2 card border-left-primary shadow h-10 py-2 ">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="mb-0 font-weight-bold text-primary text-uppercase mb-1">CTR
                                        </div>
                                        <div class="mb-0 font-weight-bold font-weight-bold text-primary">2544
                                        </div>
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto">
                                                <div class="mb-0 mr-3 font-weight-bold text-gray-800">26%</div>
                                            </div>
                                            <div class="col">
                                                <div class="progress progress-sm mr-2">
                                                    <div class="progress-bar bg-primary" role="progressbar"
                                                        style="width: 86%" aria-valuenow="50" aria-valuemin="0"
                                                        aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fa-solid fa-computer fa-2x text-gray-500"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <!-- Third Column -->
                    <div class="col-lg-6">
                        <!-- Grayscale Utilities -->
                        <div class="row">
                            <div class="col-6">
                                <div class="card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">CTR
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">CMC
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="my-2 card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">PGO
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="my-2 card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">CVA
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="my-2 card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">CEL
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="my-2 card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">PTO
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="my-2 card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">MGA
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="my-2 card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">LNA
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="my-2 card border-left-success shadow h-10 py-2 ">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="mb-0 font-weight-bold text-success text-uppercase mb-1">FOZ
                                                </div>
                                                <div class="mb-0 font-weight-bold font-weight-bold text-success">2544
                                                </div>
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col-auto">
                                                        <div class="mb-0 mr-3 font-weight-bold text-success">50%</div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="progress progress-sm mr-2">
                                                            <div class="progress-bar bg-success" role="progressbar"
                                                                style="width: 26%" aria-valuenow="50" aria-valuemin="0"
                                                                aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fa-solid fa-house-chimney-user fa-2x text-gray-500"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->
    </div>
@endsection
