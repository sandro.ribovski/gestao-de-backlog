<?php

use App\atividade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;



function listPolos()
{
    $listPolos = atividade::select('polo')->distinct('polo')->where('polo', '<>', "")->orderby('polo','ASC')->get();
    return $listPolos;
}


function listCidades()
{
    $listCidades = atividade::select('cidade')->where('cidade', '<>',"")->distinct('cidade')->orderby('cidade','ASC')->get();
    return $listCidades;
}


function listEmpreiteira()
{
    $listEmpreiteira = atividade::select('empreiteira')->where('empreiteira', '<>', "")->distinct('empreiteira')->orderby('empreiteira','ASC')->get();
    return $listEmpreiteira;
}


function formatarData($data)
{

    if (empty($data)) {
        return "";
    }
    $date = new DateTime($data);
    return $date->format('y-m-d');
}


function somar_dias_uteis($str_data, $int_qtd_dias_somar)
    {

        // Caso seja informado uma data do MySQL do tipo DATETIME - aaaa-mm-dd 00:00:00
        // Transforma para DATE - aaaa-mm-dd
        $str_data = substr($str_data, 0, 10);
        // Se a data estiver no formato brasileiro: dd/mm/aaaa
        // Converte-a para o padrão americano: aaaa-mm-dd
        if (preg_match("@/@", $str_data) == 1) {
            $str_data = implode("-", array_reverse(explode("/", $str_data)));
        }
        $array_data = explode('-', $str_data);
        $count_days = 0;
        $int_qtd_dias_uteis = 0;
        while ($int_qtd_dias_uteis < $int_qtd_dias_somar) {
            $count_days++;
            if (($dias_da_semana = gmdate('w', strtotime('+' . $count_days . ' day', mktime(0, 0, 0, $array_data[1], $array_data[2], $array_data[0])))) != '0' && $dias_da_semana != '6') {
                $int_qtd_dias_uteis++;
            }
        }
        return gmdate('d/m/Y', strtotime('+' . $count_days . ' day', strtotime($str_data)));
    }
