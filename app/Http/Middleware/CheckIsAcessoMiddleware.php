<?php

namespace App\Http\Middleware;

use App\Http\Controllers\BacklogController;
use Closure;
use Illuminate\Support\Facades\Auth;


class CheckIsAcessoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->perfil == 'Polo') {
            return redirect('/polo');
        }
        if (Auth::user()->perfil == 'Empreiteira') {
            return redirect('/empreiteira');
        }
        if (Auth::user()->perfil == 'Gestao' ) {
            return redirect('/gestao');
        }
        if (Auth::user()->perfil == 'Central' ) {
            return redirect('/central');
        }
        return $next($request);
    }
}
