<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIsEmpreiteiraMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::user()->perfil != 'Empreiteira') {
            dd('Você não Tem Permissão para Acessar Esta Fila');
        }
        return $next($request);
    }
}
