<?php

namespace App\Http\Controllers;

use App\atividade;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BacklogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('----------------');
        $backlog = DB::table('osptb024_osp_ativa_consolidada')->selectRaw("*")->orderBy('data_da_venda', 'desc')->get();
        $backlogCount = atividade::all()->count();
        $backlogNag = atividade::where('agendada', '=', 'Não')->count();
        $perNag = number_format($backlogNag / $backlogCount * 100, 0);
        $backlogAg = atividade::where('agendada', '=', 'Sim')->count();
        $perAg = number_format($backlogAg / $backlogCount * 100, 0);
        $backlogAtrasado = atividade::where('data_promessa', '<=', date('Y-m-d', strtotime('-1 days')))->count();
        $perAtrasado = number_format($backlogAtrasado / $backlogCount * 100, 0);
        
    }


    public function polo()
    {
        $backlog = DB::table('osptb024_osp_ativa_consolidada')->selectRaw("contrato,data_da_venda,data_promessa,data_agendada,periodo_agendado,fluxo_atual,dias_backlog,dias_atraso,cidade,polo,regional,id_atividade,tipo_atividade,
        status_atividade,empreiteira,tecnico,tipo_imovel,cliente,fone,celular,backlog_empreiteira")->orderBy('data_da_venda', 'desc')->get();
        $backlogCount = atividade::all()->count();
        $backlogNag = atividade::where('agendada', '=', 'Não')->count();
        $perNag = number_format($backlogNag / $backlogCount * 100, 0);
        $backlogAg = atividade::where('agendada', '=', 'Sim')->count();
        $perAg = number_format($backlogAg / $backlogCount * 100, 0);
        $backlogAtrasado = atividade::where('data_promessa', '<=', date('Y-m-d', strtotime('-1 days')))->count();
        $perAtrasado = number_format($backlogAtrasado / $backlogCount * 100, 0);
        return view('rncFront.polo', compact('backlog', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
    }
    public function Empreiteira()
    {
        $backlog = DB::table('osptb024_osp_ativa_consolidada')->selectRaw("contrato,data_da_venda,data_promessa,data_agendada,periodo_agendado,dias_backlog,dias_atraso,cidade,polo,regional,id_atividade,tipo_atividade,status_atividade,
        tecnico,tipo_imovel,cliente,fone,celular")->orderBy('data_da_venda', 'desc')->get();
        $backlogCount = atividade::all()->count();
        $backlogNag = atividade::where('agendada', '=', 'Não')->count();
        $perNag = number_format($backlogNag / $backlogCount * 100, 0);
        $backlogAg = atividade::where('agendada', '=', 'Sim')->count();
        $perAg = number_format($backlogAg / $backlogCount * 100, 0);
        $backlogAtrasado = atividade::where('data_promessa', '<=', date('Y-m-d', strtotime('-1 days')))->count();
        $perAtrasado = number_format($backlogAtrasado / $backlogCount * 100, 0);
        return view('rncFront.empreiteira', compact('backlog', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
    }

    public function gestao()
    {
        $backlog = DB::table('osptb024_osp_ativa_consolidada')->selectRaw("*")->orderBy('data_da_venda', 'desc')->get();
        $backlogCount = atividade::all()->count();
        $backlogNag = atividade::where('agendada', '=', 'Não')->count();
        $perNag = number_format($backlogNag / $backlogCount * 100, 0);
        $backlogAg = atividade::where('agendada', '=', 'Sim')->count();
        $perAg = number_format($backlogAg / $backlogCount * 100, 0);
        $backlogAtrasado = atividade::where('data_promessa', '<=', date('Y-m-d', strtotime('-1 days')))->count();
        $perAtrasado = number_format($backlogAtrasado / $backlogCount * 100, 0);
        return view('rncFront.list', compact('backlog', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
    }
    
    public function central()
    {
        $backlog = DB::table('osptb024_osp_ativa_consolidada')->selectRaw("*")->orderBy('data_da_venda', 'desc')->get();
        $backlogCount = atividade::all()->count();
        $backlogNag = atividade::where('agendada', '=', 'Não')->count();
        $perNag = number_format($backlogNag / $backlogCount * 100, 0);
        $backlogAg = atividade::where('agendada', '=', 'Sim')->count();
        $perAg = number_format($backlogAg / $backlogCount * 100, 0);
        $backlogAtrasado = atividade::where('data_promessa', '<=', date('Y-m-d', strtotime('-1 days')))->count();
        $perAtrasado = number_format($backlogAtrasado / $backlogCount * 100, 0);
        return view('rncFront.list', compact('backlog', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
    }


    public function users()
    {
        $users = User::all();
        return view('rncFront.users', compact('users'));
    }

    /**
     * filtro a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filtro(Request $request)
    {
        // verificação de campos vazio

        if (
            empty($request->cidade) and empty($request->polo) and empty($request->regional) and empty($request->recurso_disp_na_venda) and empty($request->fluxo_atual) and empty($request->tipo_atividade)
            and empty($request->status_atividade) and empty($request->backlog_empreiteira) and empty($request->agendada) and empty($request->despachada) and empty($request->empreiteira)
        ) {
            return redirect()->back();
        }

        $w = "";
        foreach ($request->all() as $key => $value) {
            if (!empty($value) and $key != '_token') {
                $key = str_replace('_fim', '', $key);

                $where = "$key" . '=' . "'$value'" . " and ";
                $w = $w . $where;
            }
        }
        $w = substr($w, 0, -4);


        $backlogAll = DB::select("SELECT * FROM osptb024_osp_ativa_consolidada WHERE $w");

        $backlog = DB::select("SELECT count(1) as backlogT FROM osptb024_osp_ativa_consolidada WHERE $w");
        $backlogCount =  $backlog[0]->backlogT;



        $backlogAgg = DB::select("SELECT count(1) as ag FROM osptb024_osp_ativa_consolidada WHERE $w and agendada = 'Sim'");
        $backlogAg = $backlogAgg[0]->ag;

        if ($backlogCount != 0) {
            $perAg = number_format($backlogAg / $backlogCount * 100, 0);
        } else {
            $perAg = 0;
        }

        $backlogNag = DB::select("SELECT count(1) as nag FROM osptb024_osp_ativa_consolidada WHERE $w and agendada = 'Não'");
        $backlogNag = $backlogNag[0]->nag;

        if ($backlogCount != 0) {
            $perNag = number_format($backlogNag / $backlogCount * 100, 0);
        } else {
            $perNag = 0;
        }


        $backlogAt = DB::select("SELECT count(1) as atrasado FROM osptb024_osp_ativa_consolidada WHERE $w and data_promessa < now() -1");
        $backlogAtrasado = $backlogAt[0]->atrasado;
        if ($backlogCount != 0) {
            $perAtrasado = number_format($backlogAtrasado / $backlogCount * 100, 0);
        } else {
            $perAtrasado = 0;
        }

        switch (Auth::user()->perfil) {
            case 'Empreiteira':
                return view('rncFront.listFempreiteira', compact('backlogAll', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
                break;
            case 'Central':
                return view('rncFront.listfF', compact('backlogAll', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
                break;
            case 'Polo':
                return view('rncFront.listFpolo', compact('backlogAll', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
                break;
            case 'Gestao':
                return view('rncFront.listfF', compact('backlogAll', 'backlogCount', 'backlogNag', 'perNag', 'backlogAg', 'perAg', 'backlogAtrasado', 'perAtrasado'));
                break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
