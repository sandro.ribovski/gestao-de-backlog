<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class atividade extends Model
{
  

    protected $fillable = [
        'id','contrato','data_da_venda','data_promessa','data_agendada','periodo_agendado','recurso_disp_na_venda','fluxo_atual','dias_backlog','dias_atraso','cidade',
        'polo','regional','id_atividade','tipo_atividade','status_atividade','zona_de_trabalho','cod_cidade','cod_celula','backlog_empreiteira','empreiteira','tecnico',
        'tipo_imovel','cpf_cnpj','cliente','fone','celular','agendada','despachada'];    

    protected $table = 'osptb024_osp_ativa_consolidada';
}
