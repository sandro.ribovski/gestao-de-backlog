<?php

use App\Http\Controllers\BacklogController;
use App\Http\Controllers\ReparosController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', [BacklogController::class, 'index'])->name('inicio')->middleware('auth')->middleware('check.acesso');
Route::get('/', [BacklogController::class,'index'])->name('inicio')->middleware('auth')->middleware('check.acesso');

Route::get('/polo', [BacklogController::class,'polo'])->name('polo')->middleware('auth')->middleware('check.acesso.polo');
Route::get('/empreiteira', [BacklogController::class,'empreiteira'])->name('empreiteira')->middleware('auth')->middleware('check.acesso.empreiteira');;
Route::get('/gestao', [BacklogController::class,'gestao'])->name('gestao')->middleware('auth')->middleware('check.acesso.gestao');
Route::get('/central', [BacklogController::class,'central'])->name('central')->middleware('auth')->middleware('check.acesso.central');



Route::get('/reparosIni', [ReparosController::class,'index'])->name('reparosIni')->middleware('auth');








Route::post('/filtro', [BacklogController::class,'filtro'])->name('filtro')->middleware('auth');
Route::get('/users', [BacklogController::class,'users'])->name('users')->middleware('auth')->middleware('check.acesso');